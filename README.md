# README #

This is a coding test assignment done for sampler.

### What is this repository for? ###

* Version 1.0.0

### How do I get set up? ###

* git clone https://bitbucket.org/saqibbilal/book_library.git
* cd book_library
* create a database and connect it using the settings in config/database.php file
* composer install
* run command: php artisan migrate
* run command: php artisan db:seed
* you are ready to check the APIs. Use postman collection for your convenience(included in the project).
*You are ready to go. 
  
You might need these:
* API Documentation(sample): Sampler API documentation.docx
* Postman API collection: BookStore.postman_collection.json

I could not find time to write tests with PHPUnit due to business.
### Who do I talk to? ###

* Muhammad Saqib Bilal
* saqib_bilal786@yahoo.com
* http://saqibbilal.ca

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

