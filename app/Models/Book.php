<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    protected $table = "books";
    protected $fillable = ['title', 'isbn', 'published_at', 'status'];
    protected $dateFormat = 'Y-m-d';

    public function users(){
        return $this->belongsToMany(User::class, 'user_action_logs');
    }
}
