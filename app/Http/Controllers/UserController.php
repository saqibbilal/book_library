<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function register(Request $request){
        $validator = Validator::make($request->all(),[
            "name" => "required",
            "email" => "required|email",
            "password" => "required",
        ]);
        if($validator->fails()){
            return response()->json(['status_code'=> 400, 'message' => 'Bad Request']);
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json([
            'status_code'=> 200,
            'message' => 'User registered successfully!'
        ]);
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(),[
            "email" => "required|email",
            "password" => "required",
        ]);
        if($validator->fails()){
            return response()->json(['status_code'=> 400, 'message' => 'Bad Request']);
        }

        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)){
            return response()->json([
                'status_code'=> 401,
                'message' => 'Invalid user credentials provided'
            ]);
        }
        $user = User::where('email', $request->email)->first();
        $token = $user->createToken('authToken')->plainTextToken;
        return response()->json([
            'status_code'=> 200,
            'token' => $token
        ]);
    }

    public function logout(Request $request){
        $request->user()->currentAccessToken()->delete();
        return response()->json([
            'status_code'=> 200,
            'message' => 'Logged out successfully!'
        ]);
    }


    // implement checkin/checkout logic
    public function user_action(Request $request, User $user, Book $book){
        $validator = Validator::make($request->all(),["action" => "required"]);
        if($validator->fails()){
            return response()->json(['status_code'=> 400, 'message' => 'Bad Request']);
        }
        if($request->action){
            $action = strtoupper($request->action);
            try{
                if($action === "CHECKIN" AND $book->status === 'CHECKED_OUT'){
                    if($user->books()->save($book, array(
                        'action' => $action,
                        'updated_at' => date('Y-m-d H:i:s')
                    ))){
                        $book->status = 'AVAILABLE'; //  make the book 'available' for others
                    }
                }else if($action === "CHECKIN" AND $book->status === 'AVAILABLE') {
                    return response()->json(['status_code'=> 400,'message' => "You have already returned this book."]);
                }else if ($action === "CHECKOUT"){
                    if($book->status === 'AVAILABLE'){
                        //  checkout the book
                        if($user->books()->save($book, array('action' => $action,
                            'updated_at' => date('Y-m-d H:i:s')
                        ))){
                            $book->status = 'CHECKED_OUT'; //  change status of book to CHECKED_OUT
                        }
                    }else{
                        return response()->json([
                            'status_code'=> 400,
                            'message' => "The book you requested is not available. Please try again later"
                        ]);
                    }
                }
                $book->save();
                return response()->json(['message'=>"Book $action successful",'data'=>$book],200);
            }catch (\Exception $e){
                return response()->json(['status_code'=> 422, 'message' => "Something went wrong, please try later"]);
            }
        }
    }
}
