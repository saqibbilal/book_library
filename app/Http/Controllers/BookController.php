<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Http\resources\BookResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book = Book::paginate(5);
        return BookResource::collection($book);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // not needed
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            "title" => "required",
            "isbn" => "required",
            "published_at" => "required",
            "status" => "required",
        ]);
        if($validator->fails()){
            return response()->json(['status_code'=> 400, 'message' => 'Bad Request']);
        }
        // check validity of isbn number
        $check = $this->ISBN_checker($request->isbn);
        if($check === 0){
            $book = new Book();
            $book->title = $request->title;
            $book->isbn = $request->isbn;
            $book->published_at = $request->published_at;
            $book->status = $request->status;
            if($book->save()){
                return new BookResource($book);
            }
        }else{
            return response()->json(['status_code'=> 400, 'message' => 'Invalid ISBN provided']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::findOrFail($id);
        return new BookResource($book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // not needed
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            "title" => "required",
            "isbn" => "required",
            "published_at" => "required",
            "status" => "required",
        ]);
        if($validator->fails()){
            return response()->json(['status_code'=> 400, 'message' => 'Bad Request']);
        }
        // check validity of isbn number
        $check = $this->ISBN_checker($request->isbn);
        if($check === 0){
            $book = Book::findOrFail($id);
            $book->title = $request->title;
            $book->isbn = $request->isbn;
            $book->published_at = $request->published_at;
            $book->status = $request->status;
            if($book->save()){
                return new BookResource($book);
            }
        }else{
            return response()->json(['status_code'=> 400, 'message' => 'Invalid ISBN provided']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        if($book->delete()){
            return new BookResource($book);
        }
    }

    private function ISBN_checker($isbn){
        $digits = str_split(substr($isbn, 0, 10));
        $sum = 0;
        foreach ($digits as $index => $digit)
        {
            $sum += (10 - $index) * $digit;
        }
        return $sum % 11;
    }
}
