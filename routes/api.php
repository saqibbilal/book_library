<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// not needed in the required functionality
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/books', 'App\Http\Controllers\BookController@index'); // lists all books in the library
Route::get('/books/{id}', 'App\Http\Controllers\BookController@show'); // shows detail of a particular book
Route::post('/register', 'App\Http\Controllers\UserController@register'); // user register
Route::post('/login', 'App\Http\Controllers\UserController@login'); // user login


// Below routes require a user to be logged in.
// I did not create user roles functionality to grant permissions for different tasks to different roles.
// It was beyond the scope of this assignment.
Route::group(['middleware' => ['auth:sanctum']], function(){
    Route::post('/book', 'App\Http\Controllers\BookController@store'); // create a neww book
    Route::put('/books/{id}', 'App\Http\Controllers\BookController@update'); // update a particular book
    Route::delete('/books/{id}', 'App\Http\Controllers\BookController@destroy'); // delete a book.
    Route::post('user_action/{user}/{book}', 'App\Http\Controllers\UserController@user_action'); // check_in/check_out a book
    Route::post('/logout', 'App\Http\Controllers\UserController@logout'); // logout a user
});
